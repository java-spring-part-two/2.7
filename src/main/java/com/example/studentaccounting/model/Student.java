package com.example.studentaccounting.model;

import lombok.Data;

@Data
public class Student {
    private static long idCounter = 0;
    private Long id;
    private String firstName;
    private String lastName;
    private int age;

    public Student(String firstName, String lastName, int age) {
        this.id = ++idCounter;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
}
