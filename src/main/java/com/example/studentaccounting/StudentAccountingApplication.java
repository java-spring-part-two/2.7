package com.example.studentaccounting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentAccountingApplication {

    @Value("${app.create-demo-students:false}")
    private boolean createDemoStudents;

    public static void main(String[] args) {
        SpringApplication.run(StudentAccountingApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(StudentService studentService) {
        return (args) -> {
            if (createDemoStudents) {
                studentService.addStudent(new Student("John", "Doe", 20));
                studentService.addStudent(new Student("Jane", "Doe", 22));
                // Добавьте больше студентов при необходимости
            }
        };
    }
}
