
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StudentEventListener {

    @EventListener
    public void onStudentCreated(StudentCreatedEvent event) {
        System.out.println("Created student: " + event.getStudent().toString());
    }

    @EventListener
    public void onStudentDeleted(StudentDeletedEvent event) {
        System.out.println("Deleted student with ID: " + event.getStudentId());
    }
}
