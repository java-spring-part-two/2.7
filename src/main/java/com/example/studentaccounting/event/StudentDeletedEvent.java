
package com.example.studentaccounting.event;

public class StudentDeletedEvent extends ApplicationEvent {
    private Long studentId;

    public StudentDeletedEvent(Object source, Long studentId) {
        super(source);
        this.studentId = studentId;
    }

    public Long getStudentId() {
        return studentId;
    }
}
