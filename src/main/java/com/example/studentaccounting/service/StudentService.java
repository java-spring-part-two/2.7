package com.example.studentaccounting.service;

import com.example.studentaccounting.model.Student;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class StudentService {
    private final Map<Long, Student> students = new HashMap<>();

    public Student getStudent(Long id) {
        return students.get(id);
    }

    public Map<Long, Student> getAllStudents() {
        return students;
    }

    public void clearStudents() {
        students.clear();
    }

    public void addStudent(Student student) {
        students.put(student.getId(), student);
        eventPublisher.publishEvent(new StudentCreatedEvent(this, student));
    }

    public void deleteStudent(Long id) {
        students.remove(id);
        eventPublisher.publishEvent(new StudentDeletedEvent(this, id));
    }
}
