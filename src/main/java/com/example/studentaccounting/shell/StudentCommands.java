package com.example.studentaccounting.shell;

import com.example.studentaccounting.model.Student;
import com.example.studentaccounting.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class StudentCommands {

    @Autowired
    private StudentService studentService;

    @ShellMethod("Add a new student")
    public String addStudent(String firstName, String lastName, int age) {
        Student student = new Student(firstName, lastName, age);
        studentService.addStudent(student);
        return "Added student: " + student.toString();
    }

    @ShellMethod("Delete a student by ID")
    public String deleteStudent(Long id) {
        studentService.deleteStudent(id);
        return "Deleted student with ID: " + id;
    }

    @ShellMethod("List all students")
    public String listStudents() {
        return studentService.getAllStudents().values().toString();
    }

    @ShellMethod("Clear all students")
    public String clearStudents() {
        studentService.clearStudents();
        return "All students have been cleared.";
    }
}
